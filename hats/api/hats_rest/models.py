from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return (
            f"{self.fabric} - {self.style_name} "
            f"- {self.color} - {self.location}"
        )

    class Meta:
        ordering = ("fabric", "style_name", "color")
