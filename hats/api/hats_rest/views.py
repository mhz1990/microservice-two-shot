from django.shortcuts import render
from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href"]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    def default(self, obj):
        if isinstance(obj, LocationVO):
            return {
                "import_href": obj.import_href,
                # Add more properties if needed
            }
        return super().default(obj)


@require_http_methods(["GET", "POST"])
def api_hats_list(request, location_id=None):

    if request.method == "GET":
        import_href = f"/api/locations/{location_id}/"
        location_vo = LocationVO.objects.get(import_href=import_href)
        if location_vo.id is not None:
            hats = Hat.objects.filter(location=location_vo.id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            import_href = f"/api/locations/{location_id}/"
            location_vo = LocationVO.objects.get(import_href=import_href)
            content["location"] = location_vo
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )
