import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route
            path="hats"
            element={<HatList hats={props.hats} />}
          />

          <Route
            path="shoes"
            element={<ShoeList shoes={props.shoes} />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
