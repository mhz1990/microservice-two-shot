import React from 'react';

function HatList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style Name</th>
          <th>Color</th>
          <th>Fabric</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map((hat) => {
          return (
            <tr key={hat.id}>

              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>{hat.fabric}</td>
              <td>
                <img src={hat.picture_url} alt={hat.style_name} style={{ height: '50px' }} />
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  );
}

export default HatList;
