import React from 'react'

function ShoeList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>color</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map((shoe) => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>
                                <img src={shoe.picture_url} alt={shoe.model_name} style={{ height: '50px' }} />
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

export default ShoeList;
