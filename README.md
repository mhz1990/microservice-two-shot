# Wardrobify

Team:

* Matt Hoadley - Shoes Microservice
* Wilbert Machuca - Hats Microservice

## Design

## Shoes microservice

1 June 2023: Shoes app added to installed apps in project settings. Shoe List view and encoder created. List Shoes URL created and added to app and projects URLs.py. Shoes model and BinVO created. Poller successfully polling data from Bin in Wardrobe.

2 June 2023: Now able to create shoe item and view shoe item in bin in Insomnia. Created ShoeList.js and updated index.js for shoes. Successfully printing shoe data to console in react application.

## Hats microservice
Hats microservice is made of 2 app which are configured to poll data of locations every 60s from the wardrobe. The location  data is used as an instance in the hat model. As parts of properties in the hat model. The microservice uses view functions to interact in 5 main browser events to serialize data and send it in JsonResponse. View functions are properly mapped in urls and their routes.
