from django.urls import path
from .views import api_shoes_list


urlpatterns = [
    path("shoes/", api_shoes_list, name="api_create_shoe"),
    path("bins/<int:bin_id>/shoes/", api_shoes_list, name="api_shoes_list"),
    # path("bins/<int:pk>/", api_show_shoe, name="api_shoe_detail"),
]
