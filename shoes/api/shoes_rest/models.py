from django.db import models
from django.urls import reverse

# from wardrobe.api.wardrobe_api.models import Bin


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)


# Create your models here.
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name} - {self.color} - {self.picture_url} - {self.bin}"
        # return f"{self.manufacturer} - {self.model_name} - {self.color} - {self.picture_url} - {self.bin}"

    class Meta:
        ordering = ("manufacturer", "model_name", "color", "picture_url", "bin")
        # ordering = ("manufacturer", "model_name", "color", "picture_url", "bin")
