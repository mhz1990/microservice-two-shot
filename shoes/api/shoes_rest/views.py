# from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO


# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href"]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    def default(self, obj):
        if isinstance(obj, BinVO):
            return {
                "import_href": obj.import_href,
                # Add more properties if needed
            }
        return super().default(obj)


@require_http_methods(["GET", "POST"])
def api_shoes_list(request, bin_id=None):

    if request.method == "GET":
        import_href = f"/api/bins/{bin_id}/"
        bin_vo = BinVO.objects.get(import_href=import_href)
        if bin_vo is not None:
            shoes = Shoe.objects.filter(bin=bin_vo.id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            import_href = f"/api/bins/{bin_id}/"
            bin_vo = BinVO.objects.get(import_href=import_href)
            content["bin"] = bin_vo
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
